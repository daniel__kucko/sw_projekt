/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.othr.kucko.paydudekucko.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author kucko
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"email"}))
public class Customer extends SingleIdEntity implements Serializable {
    
    @OneToOne(fetch=FetchType.EAGER)
    private PaydudeAccount account;
    @OneToMany
    private List<InternalTransaction> internalTransactions;
    @OneToMany
    private List<ExternalTransaction> externalTransactions;
    
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private Address address; // TODO: 

    public Customer(String firstName, String lastName, String email, String password, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.address = address;
        this.internalTransactions = new ArrayList<>();
        this.externalTransactions = new ArrayList<>();
    }

    public Customer() {
    }

    public PaydudeAccount getAccount() {
        return account;
    }

    public void setAccount(PaydudeAccount account) {
        this.account = account;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<InternalTransaction> getInternalTransactions() {
        return internalTransactions;
    }

    public void setInternalTransactions(List<InternalTransaction> internalTransactions) {
        this.internalTransactions = internalTransactions;
    }

    public List<ExternalTransaction> getExternalTransactions() {
        return externalTransactions;
    }

    public void setExternalTransactions(List<ExternalTransaction> externalTransactions) {
        this.externalTransactions = externalTransactions;
    }
    
    public void addInternalTransaction(InternalTransaction it) {
        this.internalTransactions.add(it);
    }
    
    public void addExternalTransaction(ExternalTransaction et) {
        this.externalTransactions.add(et);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    
}
