/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.othr.kucko.paydudekucko.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author kucko
 */
@Entity
public class InternalTransaction extends SingleIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @ManyToOne(fetch=FetchType.EAGER)
    private PaydudeAccount sender;
    @ManyToOne(fetch=FetchType.EAGER)
    private PaydudeAccount receiver;
    @Temporal(TemporalType.DATE)
    private Date transactionDate;
    private long amount;

    public InternalTransaction() {
        this.transactionDate = new Date();
    }

    public InternalTransaction(PaydudeAccount sender, PaydudeAccount receiver, long amount) {
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        this.transactionDate = new Date();
    }

    public PaydudeAccount getSender() {
        return sender;
    }

    public void setSender(PaydudeAccount sender) {
        this.sender = sender;
    }

    public PaydudeAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(PaydudeAccount receiver) {
        this.receiver = receiver;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
    
}
