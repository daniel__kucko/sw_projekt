/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.othr.kucko.paydudekucko.entity;

import java.io.Serializable;
import javax.persistence.Entity;


/**
 *
 * @author kucko
 */
@Entity
public class BankAccount extends SingleIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Owner;
    private String IBAN;

    public BankAccount() {
    }

    public BankAccount(String Owner, String IBAN) {
        this.Owner = Owner;
        this.IBAN = IBAN;
    }
    
    public String getOwner() {
        return Owner;
    }

    public void setOwner(String Owner) {
        this.Owner = Owner;
    }

    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }
    
}
