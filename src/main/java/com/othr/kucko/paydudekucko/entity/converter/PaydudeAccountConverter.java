/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.othr.kucko.paydudekucko.entity.converter;

import com.othr.kucko.paydudekucko.entity.PaydudeAccount;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kucko
 */
@RequestScoped
@FacesConverter("com.othr.kucko.paydudekucko.entity.converter.PaydudeAccountConverter")
public class PaydudeAccountConverter implements Converter, Serializable {
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value){
        if (value == null){
            return "";
        }
        PaydudeAccount acc = em.find(PaydudeAccount.class, (Long) Long.parseLong(value));
        if (acc == null){
            return "";
        }
        return acc;
    }
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value){
        if(value == null){
            return null;
        }
        if(!value.getClass().equals(PaydudeAccount.class)){
            return null;
        }
        return ((PaydudeAccount)value).getId().toString();
    }
    
}
