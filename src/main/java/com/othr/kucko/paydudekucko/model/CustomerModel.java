package com.othr.kucko.paydudekucko.model;

import com.othr.kucko.paydudekucko.entity.Address;
import com.othr.kucko.paydudekucko.entity.BankAccount;
import com.othr.kucko.paydudekucko.entity.Customer;
import com.othr.kucko.paydudekucko.entity.ExternalTransaction;
import com.othr.kucko.paydudekucko.entity.InternalTransaction;
import com.othr.kucko.paydudekucko.entity.PaydudeAccount;
import com.othr.kucko.paydudekucko.entity.converter.PaydudeAccountConverter;
import com.othr.kucko.paydudekucko.service.AccountService;
import com.othr.kucko.paydudekucko.service.CustomerService;
import com.othr.kucko.paydudekucko.service.TransactionService;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class CustomerModel implements Serializable{

    private String email;
    private String password;
    private boolean loggedIn;
    private String password2;
    private String firstName;
    private String lastName;
    private String street;
    private String number;
    private String postalCode;
    private String city;
    private Customer customer;
    private List<InternalTransaction> it;
    private List<ExternalTransaction> et;
    private PaydudeAccount acc;
    private BankAccount bAcc;
    private String iban;
    private String owner;
    private Long recvID;
    private String recvIBAN;
    private long amount;
    private String usage;
    private long balance;
    private PaydudeAccount receiverAccount;
    private List<PaydudeAccount> accountList;
     
    
    
    @Inject
    private CustomerService cs;
    @Inject
    private TransactionService ts;
    @Inject
    private AccountService as;
    @Inject
    private PaydudeAccountConverter converter;
    
    public CustomerModel() {
    }

    public String getPassword2() {
        return password2;
    }

    public Long getRecvID() {
        return recvID;
    }

    public void setRecvID(Long recvID) {
        this.recvID = recvID;
    }

    public String getRecvIBAN() {
        return recvIBAN;
    }

    public void setRecvIBAN(String recvIBAN) {
        this.recvIBAN = recvIBAN;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
        
    public String login(){
        customer = cs.login(email, password);
        acc = customer.getAccount();
        it = ts.getInternalTransactions(acc.getId());
        et = ts.getExternalTransactions(acc.getId());
        bAcc = acc.getBankAccount();
        balance = acc.getBalance();
        if (bAcc == null){
            return "account.xhtml";
        } else {
            return "home.xhtml";
        }
    }
    
    public String logout(){
        loggedIn = cs.logout();
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "index.xhtml?faces-redirect=true";
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void makeInternalTransaction() {
        if (amount >= 0){
            InternalTransaction i = ts.createInternalTransaction(acc.getId(), receiverAccount.getId(), amount);
            balance -= amount;
            //it.add(cs.addInternalTransaction(customer.getId(), i.getId()));
            it = ts.getInternalTransactions(acc.getId());
        } else {
            /*Fehler*/
        }
    }
    
    public void makeExternalTransaction() {
        if (amount >= 0){
            ExternalTransaction e = ts.createExternalTransaction(acc.getId(), recvIBAN, amount, usage);
            balance -= amount;
            et.add(cs.addExternalTransaction(customer.getId(), e.getId()));
        } else {
            /*Fehler*/
        }
    }   
    
    public String register(){
        if (password.equals(password2) && password != null){
            Address address = new Address(street, number, postalCode, city);
            customer = cs.createCustomer(firstName, lastName, email, password, address);
            acc = customer.getAccount();
            it = ts.getInternalTransactions(acc.getId());
            et = ts.getExternalTransactions(acc.getId());
            bAcc = acc.getBankAccount();
            balance = acc.getBalance();
            if (bAcc == null){
                return "account.xhtml";
            } else {
                return "home.xhtml";
            }
        } else {
            /*Exception?Fehler?Nachricht?*/
            return "index.html";
        }
    }
    
    public String createBankAccount(){
        if (/*BANK API CALL*/ true){
            bAcc = as.createBankAccount(this.iban, this.owner);
            as.addBankAccountToPaydudeAccount(acc.getId(), bAcc.getId());
            return "home.xhtml";
        } else {
            return "account.xhtml";
        }
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String passwort) {
        this.password = passwort;
    }

    public List<InternalTransaction> getIt() {
        return it;
    }

    public void setIt(List<InternalTransaction> it) {
        this.it = it;
    }

    public List<ExternalTransaction> getEt() {
        return et;
    }

    public void setEt(List<ExternalTransaction> et) {
        this.et = et;
    }

    public PaydudeAccount getAcc() {
        return acc;
    }

    public void setAcc(PaydudeAccount acc) {
        this.acc = acc;
    }
       
    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public BankAccount getbAcc() {
        return bAcc;
    }

    public void setbAcc(BankAccount bAcc) {
        this.bAcc = bAcc;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }   

    public PaydudeAccount getReceiverAccount() {
        return receiverAccount;
    }

    public void setReceiverAccount(PaydudeAccount receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    public List<PaydudeAccount> getAccountList() {
        this.accountList = as.getPaydudeAccounts();
        return accountList;
    }

    public void setAccountList(List<PaydudeAccount> accountList) {
        this.accountList = as.getPaydudeAccounts();
        System.out.println(this.accountList);
    }

    public PaydudeAccountConverter getConverter() {
        return converter;
    }

    public void setConverter(PaydudeAccountConverter converter) {
        this.converter = converter;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    
    
}
