/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.othr.kucko.paydudekucko.service;

import com.othr.kucko.paydudekucko.entity.Address;
import com.othr.kucko.paydudekucko.entity.Customer;
import com.othr.kucko.paydudekucko.entity.ExternalTransaction;
import com.othr.kucko.paydudekucko.entity.InternalTransaction;
import com.othr.kucko.paydudekucko.entity.PaydudeAccount;
import java.io.Serializable;
import java.util.logging.Level;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.logging.Logger;

/**
 *
 * @author kucko
 */
@RequestScoped
public class CustomerService implements Serializable{
    
    @PersistenceContext
    private EntityManager em;
    
    @Inject
    private AccountService accS;
    @Inject
    private TransactionService tS;
    @Inject
    private Logger logger;
    
    @Transactional(Transactional.TxType.REQUIRED)
    public Customer createCustomer(String firstName, String lastName, String email, String password, Address address) {
        Customer cust = new Customer(firstName, lastName, email, password, address);
        PaydudeAccount pAcc = accS.createPaydudeAccount();
        cust.setAccount(pAcc);
        pAcc.setOwner(cust);
        em.persist(cust);
        em.persist(pAcc);
        Customer c = em.merge(cust);
        logger.log(Level.INFO, "Customer {0} was created successfully!", c);
        return c;
    }
    
    public Customer login(String email, String password) {
        Query q = em.createQuery("Select c From Customer as c where c.email = "
                + ":email").setParameter("email", email);
        Customer cust = (Customer) q.getSingleResult();
        if(password.equals(cust.getPassword())){
            return cust;
        }
        return null;
    }
    
    @Transactional
    public InternalTransaction addInternalTransaction(Long custID, Long itID){
        Customer cust = em.find(Customer.class, custID);
        InternalTransaction it = em.find(InternalTransaction.class, itID);
        cust.addInternalTransaction(it);
        em.persist(cust);
        logger.log(Level.INFO, "Added internal transaction {0} to Customer {1}", new Object[]{it, cust});
        return em.merge(it);
    }
    
    @Transactional
    public ExternalTransaction addExternalTransaction(Long custID, Long etID){
        Customer cust = em.find(Customer.class, custID);
        ExternalTransaction et = em.find(ExternalTransaction.class, etID);
        cust.addExternalTransaction(et);
        em.persist(cust);
        logger.log(Level.INFO, "Added external transaction {0} to Customer {1}", new Object[]{et, cust});
        return em.merge(et);
    }
    
    public boolean logout() {
        return false;
    }
    
}
