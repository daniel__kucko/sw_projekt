/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.othr.kucko.paydudekucko.service;

/**
 *
 * @author kucko
 */
public interface BankInterface {
    
    public boolean checkIBAN(String iban);
    
    public boolean createBankTransfer(String senderIban, String receiverIban, long amount, String usage);
    
}
