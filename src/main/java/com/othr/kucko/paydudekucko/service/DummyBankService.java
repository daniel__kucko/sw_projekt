/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.othr.kucko.paydudekucko.service;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;

/**
 *
 * @author kucko
 */
@RequestScoped
public class DummyBankService implements BankInterface, Serializable {

    @Override
    public boolean checkIBAN(String iban) {
        return true;
    }

    @Override
    public boolean createBankTransfer(String SenderIban, String receiverIban, long amount, String usage) {
        return true;
    }
    
    
    
}
