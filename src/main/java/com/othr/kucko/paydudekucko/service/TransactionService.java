/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.othr.kucko.paydudekucko.service;

import com.othr.kucko.paydudekucko.entity.BankAccount;
import com.othr.kucko.paydudekucko.entity.Customer;
import com.othr.kucko.paydudekucko.entity.ExternalTransaction;
import com.othr.kucko.paydudekucko.entity.InternalTransaction;
import com.othr.kucko.paydudekucko.entity.PaydudeAccount;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 *
 * @author kucko
 */
@RequestScoped
@WebService
public class TransactionService implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private final String url;

    @Inject
    BankInterface bankService;
    @Inject
    private Logger logger;
    @Inject
    private CustomerService cs;
    
    public TransactionService() {
        url = "";
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public InternalTransaction createInternalTransaction(Long senderID, Long receiverID, long amount) {
        PaydudeAccount sender = em.find(PaydudeAccount.class, senderID);
        sender.setBalance(sender.getBalance() - amount);
        em.persist(sender);
        PaydudeAccount receiver = em.find(PaydudeAccount.class, receiverID);
        receiver.setBalance(receiver.getBalance() + amount);
        em.persist(receiver);
        InternalTransaction ta = new InternalTransaction(sender, receiver, amount);
        em.persist(ta);
        ta = em.merge(ta);
        logger.log(Level.INFO, "Transaction successful from {0} to {1}", new Object[]{sender.toString(), receiver.toString()});
        return ta;
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public ExternalTransaction createExternalTransaction(Long senderID, String IBAN, long amount, String usage) {
        PaydudeAccount sender = em.find(PaydudeAccount.class, senderID);
        BankAccount acc = em.find(BankAccount.class, sender.getBankAccount().getId());

        if (bankService.createBankTransfer(acc.getIBAN(), IBAN, amount, usage)) {
            ExternalTransaction ta = new ExternalTransaction(sender, IBAN, amount);
            em.persist(ta);
            ta = em.merge(ta);
            logger.log(Level.INFO, "Transaction successful from {0} to IBAN {1}", new Object[]{sender, IBAN});
            return ta;
        } else {
            return null;
            /*Throw Exception?*/
        }
    }
    
    @WebMethod
    @Transactional(Transactional.TxType.REQUIRED)
    public void receiveExternalTransaction(String email, String password, long amount, String IBAN, String usage) {
        Customer cust = this.cs.login(email, password);
        if (cust == null){
            /*Exception*/
        } else {
            this.createExternalTransaction(cust.getAccount().getId(), IBAN, amount, usage);
        }
    }

    public List<InternalTransaction> getInternalTransactions(Long senderID) {
        PaydudeAccount sender = em.find(PaydudeAccount.class, senderID);
        Query q = em.createQuery("Select t From InternalTransaction as t where t.sender = "
                + ":sender OR t.receiver = :receiver order by t.transactionDate desc").setParameter("sender", sender).setParameter("receiver", sender);
        //Query q2 = em.createQuery("Select t From InternalTransaction as t where t.receiver = "
        //        + ":receiver").setParameter("receiver", sender);
        List<InternalTransaction> result = q.getResultList();
        //result.addAll(q2.getResultList());
        return result;
    }

    public List<ExternalTransaction> getExternalTransactions(Long senderID) {
        PaydudeAccount sender = em.find(PaydudeAccount.class, senderID);
        Query q = em.createQuery("Select t From ExternalTransaction as t where t.sender = "
                + ":sender").setParameter("sender", sender);
        List<ExternalTransaction> result = q.getResultList();
        return result;
    }

}
