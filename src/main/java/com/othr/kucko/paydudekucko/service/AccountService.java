package com.othr.kucko.paydudekucko.service;

import com.othr.kucko.paydudekucko.entity.BankAccount;
import com.othr.kucko.paydudekucko.entity.PaydudeAccount;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@RequestScoped
public class AccountService implements Serializable {
    
    @PersistenceContext
    private EntityManager em;
    
    @Inject
    private Logger logger;
    
    public AccountService(){
        
    }
    
    public List<PaydudeAccount> getPaydudeAccounts(){
        Query q = em.createQuery("Select a from PaydudeAccount as a");
        return q.getResultList();
    }
    
    @Transactional(Transactional.TxType.REQUIRED)
    public PaydudeAccount createPaydudeAccount(){
        PaydudeAccount acc = new PaydudeAccount();
        em.persist(acc);
        acc = em.merge(acc);
        logger.log(Level.INFO, "Account {0} was created successfully!", acc);
        return acc;
    }
    
    @Transactional(Transactional.TxType.REQUIRED)
    public BankAccount createBankAccount(String IBAN, String Owner){
        /*
        Call the Bank API to verify the IBAN
        */
        BankAccount acc = new BankAccount(Owner, IBAN);
        em.persist(acc);
        acc = em.merge(acc);
        logger.log(Level.INFO, "Bank Account {0} was created successfully!", acc);
        return acc;
    }
    
    @Transactional(Transactional.TxType.REQUIRED)
    public void addBankAccountToPaydudeAccount(Long pAccID, Long bAccID){
        PaydudeAccount pAccount = em.find(PaydudeAccount.class, pAccID);
        BankAccount bAccount = em.find(BankAccount.class, bAccID);
        pAccount.setBankAccount(bAccount);
        em.persist(pAccount);
        logger.log(Level.INFO, "Bank Account {0} was added to Account {1}", new Object[]{bAccount, pAccount});
    }
    
    
}
