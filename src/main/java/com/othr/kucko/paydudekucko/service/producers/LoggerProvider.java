/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.othr.kucko.paydudekucko.service.producers;

import java.util.logging.Logger;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 *
 * @author kucko
 */

@Dependent
public class LoggerProvider {
    
    @Produces @Dependent
    public Logger produceLogger(InjectionPoint p){
        return Logger.getLogger("PaydudeKucko-"+p.getMember().getDeclaringClass().getSimpleName());
    }
    
}
