package com.othr.kucko.paydudekucko.entity;

import com.othr.kucko.paydudekucko.entity.Address;
import com.othr.kucko.paydudekucko.entity.ExternalTransaction;
import com.othr.kucko.paydudekucko.entity.InternalTransaction;
import com.othr.kucko.paydudekucko.entity.PaydudeAccount;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-23T18:56:52")
@StaticMetamodel(Customer.class)
public class Customer_ extends SingleIdEntity_ {

    public static volatile SingularAttribute<Customer, String> firstName;
    public static volatile SingularAttribute<Customer, String> lastName;
    public static volatile SingularAttribute<Customer, String> password;
    public static volatile SingularAttribute<Customer, Address> address;
    public static volatile ListAttribute<Customer, InternalTransaction> internalTransactions;
    public static volatile ListAttribute<Customer, ExternalTransaction> externalTransactions;
    public static volatile SingularAttribute<Customer, PaydudeAccount> account;
    public static volatile SingularAttribute<Customer, String> email;

}