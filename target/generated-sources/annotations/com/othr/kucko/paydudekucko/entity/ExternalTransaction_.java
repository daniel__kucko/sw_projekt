package com.othr.kucko.paydudekucko.entity;

import com.othr.kucko.paydudekucko.entity.PaydudeAccount;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-23T18:56:52")
@StaticMetamodel(ExternalTransaction.class)
public class ExternalTransaction_ extends SingleIdEntity_ {

    public static volatile SingularAttribute<ExternalTransaction, Long> amount;
    public static volatile SingularAttribute<ExternalTransaction, PaydudeAccount> sender;
    public static volatile SingularAttribute<ExternalTransaction, String> receiverIBAN;
    public static volatile SingularAttribute<ExternalTransaction, Date> transactionDate;

}