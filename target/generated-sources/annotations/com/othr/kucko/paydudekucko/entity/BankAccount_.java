package com.othr.kucko.paydudekucko.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-23T18:56:52")
@StaticMetamodel(BankAccount.class)
public class BankAccount_ extends SingleIdEntity_ {

    public static volatile SingularAttribute<BankAccount, String> Owner;
    public static volatile SingularAttribute<BankAccount, String> IBAN;

}