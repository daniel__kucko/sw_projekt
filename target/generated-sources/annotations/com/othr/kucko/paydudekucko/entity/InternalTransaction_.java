package com.othr.kucko.paydudekucko.entity;

import com.othr.kucko.paydudekucko.entity.PaydudeAccount;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-23T18:56:52")
@StaticMetamodel(InternalTransaction.class)
public class InternalTransaction_ extends SingleIdEntity_ {

    public static volatile SingularAttribute<InternalTransaction, Long> amount;
    public static volatile SingularAttribute<InternalTransaction, PaydudeAccount> receiver;
    public static volatile SingularAttribute<InternalTransaction, PaydudeAccount> sender;
    public static volatile SingularAttribute<InternalTransaction, Date> transactionDate;

}