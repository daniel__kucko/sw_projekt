package com.othr.kucko.paydudekucko.entity;

import com.othr.kucko.paydudekucko.entity.BankAccount;
import com.othr.kucko.paydudekucko.entity.Customer;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-01-23T18:56:52")
@StaticMetamodel(PaydudeAccount.class)
public class PaydudeAccount_ { 

    public static volatile SingularAttribute<PaydudeAccount, BankAccount> bankAccount;
    public static volatile SingularAttribute<PaydudeAccount, Customer> Owner;
    public static volatile SingularAttribute<PaydudeAccount, Long> balance;
    public static volatile SingularAttribute<PaydudeAccount, Long> id;

}